#include "mem.h"
#include <stdio.h>

void test_memory_allocation() {
    printf("Testing memory allocation...\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *block1 = _malloc(0);
    if (block1 != NULL) {
        printf("Block 1 allocated successfully.\n");
    } else {
        printf("Block 1 allocation failed.\n");
    }
    debug_heap(stdout, heap);

    _free(block1);
    debug_heap(stdout, heap);
    heap_term();
    printf("Passed\n");
}

void test_one_block_freeing() {
    printf("Testing single block freeing...\n");
    void *heap = heap_init(0);
    printf("Before:\n");
    debug_heap(stdout, heap);

    void *block = _malloc(256);
    printf("State after allocation:\n");
    debug_heap(stdout, heap);

    if (block != NULL) {
        printf("Block allocated successfully.\n");
    } else {
        printf("Block allocation failed.\n");
    }

    _free(block);
    printf("After freeing:\n");
    debug_heap(stdout, heap);
    heap_term();
    printf("Passed\n");
}

void test_three_block_freeing() {
    printf("Testing three block freeing...\n");
    void *heap = heap_init(0);
    printf("Before:\n");
    debug_heap(stdout, heap);

    void *block1 = _malloc(256);
    void *block2 = _malloc(256);
    void *block3 = _malloc(256);

    printf("State after allocation:\n");
    debug_heap(stdout, heap);

    if (block1 != NULL && block2 != NULL && block3 != NULL) {
        printf("All blocks allocated successfully.\n");
    } else {
        printf("Block allocation failed.\n");
    }

    _free(block1);
    _free(block2);
    _free(block3);

    printf("After freeing:\n");
    debug_heap(stdout, heap);
    heap_term();
    printf("Passed\n");
}

void test_region_extension() {
    printf("Testing region extension...\n");
    void *heap = heap_init(28);
    if (heap != NULL) {
        printf("Heap initialized successfully.\n");
    } else {
        printf("Heap initialization failed.\n");
    }

    void *allocated_block = _malloc(100);
    if (allocated_block != NULL) {
        printf("Block allocated successfully.\n");
    } else {
        printf("Block allocation failed.\n");
    }

    heap_term();
    printf("Passed\n");
}

void test_memory_expansion_with_address_limitation() {
    printf("Testing memory expansion with address limitation...\n");
    void *heap = heap_init(0);
    void *block1 = _malloc(100000);
    void *block2 = _malloc(100000);

    if (block1 != NULL && block2 != NULL) {
        printf("Both blocks allocated successfully.\n");
    } else {
        printf("Block allocation failed.\n");
    }

    debug_heap(stdout, heap);
    _free(block1);
    void *block3 = _malloc(200000);
    if (block3 != NULL) {
        printf("Block 3 allocated successfully.\n");
    } else {
        printf("Block 3 allocation failed.\n");
    }
    debug_heap(stdout, heap);

    _free(block2);
    _free(block3);
    debug_heap(stdout, heap);
    heap_term();
    printf("Passed\n");
}

int main() {
    test_memory_allocation();
    test_one_block_freeing();
    test_three_block_freeing();
    test_region_extension();
    test_memory_expansion_with_address_limitation();

    return 0;
}
