#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) {
    return block->capacity.bytes >= query;
}

static size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

static size_t round_pages(size_t mem) {
    return getpagesize() * pages_count(mem);
}

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) {
    return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

static void *check_region_address(void *region_address, void const *addr, size_t size) {
    if (region_address == MAP_FAILED) {
        region_address = map_pages(addr, size, 0);
    }
    return region_address;
}

static struct region alloc_region(void const *addr, size_t query) {
    size_t actual_size = region_actual_size(size_from_capacity((block_capacity) {query}).bytes);
    void *region_start = map_pages(addr, actual_size, MAP_FIXED);
    if (region_start == MAP_FAILED) {
        region_start = map_pages(addr, actual_size, 0);
        if (region_start == MAP_FAILED) return REGION_INVALID;
    }
    block_init(region_start, (block_size) {actual_size}, NULL);
    return (struct region) {.addr = region_start, .size = actual_size, .extends = addr == region_start};
}

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

void heap_term() {
    struct block_header *current_block = (struct block_header *) HEAP_START;
    while (current_block != NULL) {
        struct block_header *next_block = current_block->next;
        while (next_block && blocks_continuous(current_block, next_block)) {
            current_block->capacity.bytes += next_block->capacity.bytes;
            current_block->next = next_block->next;
            next_block = current_block->next;
        }
        munmap(current_block, size_from_capacity(current_block->capacity).bytes);
        current_block = next_block;
    }
}

#define BLOCK_MIN_CAPACITY 24

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t capacity) {
    capacity = size_max(capacity, BLOCK_MIN_CAPACITY);
    if (block == NULL || !block_splittable(block, capacity)) return false;
    struct block_header *block_1 = block;
    block_size block_1_size = size_from_capacity((block_capacity) {capacity});
    struct block_header *block_2 = (void *) block_1 + block_1_size.bytes;
    block_size block_2_size = {size_from_capacity(block->capacity).bytes - block_1_size.bytes};
    block_init(block_2, block_2_size, block->next);
    block_init(block_1, block_1_size, block_2);
    return true;
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    if (!block || !block->next || !mergeable(block, block->next))
        return false;
    block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
    block->next = block->next->next;
    return true;
}

struct block_search_result {
    enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
    struct block_header *block;
};

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    if (!block) return (struct block_search_result) {.type = BSR_CORRUPTED, .block = NULL};
    struct block_header *prev = NULL;
    while (block) {
        while (try_merge_with_next(block));
        if (block->is_free && block_is_big_enough(sz, block)) {
            return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
        }
        prev = block;
        block = block->next;
    }
    return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = prev};
}

static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result result = find_good_or_last(block, query);
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(result.block, query);
        result.block->is_free = false;
    }
    return result;
}

static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    const struct block_header *block = block_after(last);
    const struct region region = alloc_region(block, query);

    if (region_is_invalid(&region)) return NULL;

    last->next = region.addr;

    if (try_merge_with_next(last)) {
        return last;
    }

    return last->next;
}

static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    if (heap_start == NULL || query <= 0) {
        return NULL;
    }
    size_t query_for_memalloc = (query > BLOCK_MIN_CAPACITY) ? query : BLOCK_MIN_CAPACITY;
    struct block_search_result result = try_memalloc_existing(query_for_memalloc, heap_start);
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        return result.block;
    }
    if (result.type == BSR_CORRUPTED) {
        return NULL;
    }
    struct block_header *heap = grow_heap(result.block, query_for_memalloc);
    result = try_memalloc_existing(query_for_memalloc, heap);
    return (result.type == BSR_FOUND_GOOD_BLOCK) ? result.block : NULL;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}